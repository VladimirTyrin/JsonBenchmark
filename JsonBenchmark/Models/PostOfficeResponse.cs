﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JsonBenchmark.Models
{
    public class PostOfficeResponse
    {
        [JsonProperty("address")]
        [JsonPropertyName("address")]
        public PostOfficeAddress Address { get; set; }

        [JsonProperty("addressFias")]
        [JsonPropertyName("addressFias")]
        public PostOfficeAddressFias AddressFias { get; set; }

        [JsonProperty("ecom")]
        [JsonPropertyName("ecom")]
        public string Ecom { get; set; }

        [JsonProperty("latitude")]
        [JsonPropertyName("latitude")]
        public string Latitude { get; set; }

        [JsonProperty("longitude")]
        [JsonPropertyName("longitude")]
        public string Longitude { get; set; }

        [JsonProperty("workTime")]
        [JsonPropertyName("workTime")]
        public List<string> WorkTime { get; set; }
    }
}