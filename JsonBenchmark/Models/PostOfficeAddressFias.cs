﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JsonBenchmark.Models
{
    public class PostOfficeAddressFias
    {
        [JsonProperty("addGarCode")]
        [JsonPropertyName("addGarCode")]
        public string AddGarCode { get; set; }

        [JsonProperty("locationGarCode")]
        [JsonPropertyName("locationGarCode")]
        public string LocationGarCode { get; set; }

        [JsonProperty("regGarId")]
        [JsonPropertyName("regGarId")]
        public string RegGarId { get; set; }
    }
}