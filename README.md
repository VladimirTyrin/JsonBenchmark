```
// * Summary *

BenchmarkDotNet=v0.12.1, OS=Windows 10.0.19042
Intel Core i7-3770K CPU 3.50GHz (Ivy Bridge), 1 CPU, 8 logical and 4 physical cores
.NET Core SDK=5.0.200-preview.21079.7
  [Host]        : .NET Core 3.1.12 (CoreCLR 4.700.21.6504, CoreFX 4.700.21.6905), X64 RyuJIT
  .NET Core 3.1 : .NET Core 3.1.12 (CoreCLR 4.700.21.6504, CoreFX 4.700.21.6905), X64 RyuJIT
  .NET Core 5.0 : .NET Core 5.0.3 (CoreCLR 5.0.321.7212, CoreFX 5.0.321.7212), X64 RyuJIT


|                                  Method |           Job |       Runtime |     Mean |   Error |  StdDev |     Gen 0 |     Gen 1 |    Gen 2 |    Allocated |
|---------------------------------------- |-------------- |-------------- |---------:|--------:|--------:|----------:|----------:|---------:|-------------:|
|          NewtonsoftJsonDeserializeAsync | .NET Core 3.1 | .NET Core 3.1 | 343.2 ms | 4.71 ms | 4.41 ms | 9000.0000 | 3000.0000 |        - |  54648.16 KB |
|          SystemTextJsonDeserializeAsync | .NET Core 3.1 | .NET Core 3.1 | 293.3 ms | 2.44 ms | 2.04 ms | 8000.0000 | 3000.0000 |        - |  51633.42 KB |
|                 NewtonsoftJsonSerialize | .NET Core 3.1 | .NET Core 3.1 | 156.8 ms | 1.40 ms | 1.31 ms |  250.0000 |         - |        - |   1925.52 KB |
|       SystemTextJsonSerializeSyncStream | .NET Core 3.1 | .NET Core 3.1 | 243.1 ms | 3.63 ms | 3.22 ms |         - |         - |        - | 132109.95 KB |
| SystemTextJsonSerializeSyncBufferWriter | .NET Core 3.1 | .NET Core 3.1 | 199.5 ms | 0.37 ms | 0.33 ms |         - |         - |        - |   1200.55 KB |
|            SystemTextJsonSerializeAsync | .NET Core 3.1 | .NET Core 3.1 | 206.9 ms | 1.89 ms | 1.76 ms |         - |         - |        - |   1200.15 KB |
|          NewtonsoftJsonDeserializeAsync | .NET Core 5.0 | .NET Core 5.0 | 301.2 ms | 3.85 ms | 3.60 ms | 9000.0000 | 3000.0000 |        - |   54648.8 KB |
|          SystemTextJsonDeserializeAsync | .NET Core 5.0 | .NET Core 5.0 | 243.9 ms | 2.98 ms | 2.78 ms | 8666.6667 | 3333.3333 | 333.3333 |  51436.55 KB |
|                 NewtonsoftJsonSerialize | .NET Core 5.0 | .NET Core 5.0 | 142.8 ms | 1.37 ms | 1.28 ms |  250.0000 |         - |        - |   1925.52 KB |
|       SystemTextJsonSerializeSyncStream | .NET Core 5.0 | .NET Core 5.0 | 226.1 ms | 3.10 ms | 2.90 ms |         - |         - |        - | 131629.06 KB |
| SystemTextJsonSerializeSyncBufferWriter | .NET Core 5.0 | .NET Core 5.0 | 185.7 ms | 1.83 ms | 1.71 ms |         - |         - |        - |    720.80 KB |
|            SystemTextJsonSerializeAsync | .NET Core 5.0 | .NET Core 5.0 | 188.9 ms | 1.95 ms | 1.82 ms |         - |         - |        - |    720.89 KB |
```
