﻿using System;
using System.Buffers;
using System.IO;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using BenchmarkDotNet.Attributes;
using BenchmarkDotNet.Jobs;
using BenchmarkDotNet.Running;
using JsonBenchmark.Models;
using Newtonsoft.Json;
using JsonSerializer = Newtonsoft.Json.JsonSerializer;

namespace JsonBenchmark
{
    [MemoryDiagnoser]
    [SimpleJob(RuntimeMoniker.NetCoreApp31)]
    [SimpleJob(RuntimeMoniker.NetCoreApp50)]
    public class Benchmark
    {
        private PostOfficeListResponse _response;
        private byte[] _dataBytes;

        [GlobalSetup]
        public void Setup()
        {
            _dataBytes = File.ReadAllBytes("OPS.json");

            var text = File.ReadAllText("OPS.json");
            _response = JsonConvert.DeserializeObject<PostOfficeListResponse>(text);
        }

        private Stream CreateDataStream() => new MemoryStream(_dataBytes);

        [Benchmark]
        public PostOfficeListResponse NewtonsoftJsonDeserializeAsync()
        {
            var dataCopy = CreateDataStream();
            using var reader = new StreamReader(dataCopy);
            using var jsonReader = new JsonTextReader(reader);
            var serializer = new JsonSerializer();
            return serializer.Deserialize<PostOfficeListResponse>(jsonReader);
        }

        [Benchmark]
        public async Task<PostOfficeListResponse> SystemTextJsonDeserializeAsync()
        {
            var dataCopy = CreateDataStream();
            return await System.Text.Json.JsonSerializer.DeserializeAsync<PostOfficeListResponse>(dataCopy);
        }

        [Benchmark]
        public void NewtonsoftJsonSerialize()
        {
            using var reader = new StreamWriter(Stream.Null, Encoding.UTF8);
            using var jsonWriter = new JsonTextWriter(reader);
            var serializer = new JsonSerializer();
            serializer.Serialize(jsonWriter, _response);
        }

        [Benchmark]
        public void SystemTextJsonSerializeSyncStream()
        {
            System.Text.Json.JsonSerializer.Serialize(new Utf8JsonWriter(Stream.Null), _response);
        }

        [Benchmark]
        public void SystemTextJsonSerializeSyncBufferWriter()
        {
            System.Text.Json.JsonSerializer.Serialize(new Utf8JsonWriter(NoOpBufferWriter.Instance), _response);
        }

        [Benchmark]
        public Task SystemTextJsonSerializeAsync()
        {
            return System.Text.Json.JsonSerializer.SerializeAsync(Stream.Null, _response);
        }

        private sealed class NoOpBufferWriter : IBufferWriter<byte>
        {
            public static NoOpBufferWriter Instance { get; } = new ();

            private byte[] _array;

            public void Advance(int count)
            {
                if (_array != null)
                {
                    ArrayPool<byte>.Shared.Return(_array);
                    _array = null;
                }
            }

            public Memory<byte> GetMemory(int sizeHint)
            {
                if (_array != null)
                    throw new InvalidOperationException();

                _array = ArrayPool<byte>.Shared.Rent(sizeHint);
                return _array;
            }

            public Span<byte> GetSpan(int sizeHint)
            {
                if (_array != null)
                    throw new InvalidOperationException();

                _array = ArrayPool<byte>.Shared.Rent(sizeHint);
                return _array;
            }
        }
    }

    internal static class Program
    {
        private static void Main()
        {
            BenchmarkRunner.Run<Benchmark>();
        }
    }
}
