﻿using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JsonBenchmark.Models
{
    public class PostOfficeAddress
    {
        [JsonProperty("addressType")]
        [JsonPropertyName("addressType")]
        public string AddressType { get; set; }

        [JsonProperty("area")]
        [JsonPropertyName("area")]
        public string Area { get; set; }

        [JsonProperty("house")]
        [JsonPropertyName("house")]
        public string House { get; set; }

        [JsonProperty("index")]
        [JsonPropertyName("index")]
        public string Index { get; set; }

        [JsonProperty("manualInput")]
        [JsonPropertyName("manualInput")]
        public bool? ManualInput { get; set; }

        [JsonProperty("place")]
        [JsonPropertyName("place")]
        public string Place { get; set; }

        [JsonProperty("region")]
        [JsonPropertyName("region")]
        public string Region { get; set; }

        [JsonProperty("street")]
        [JsonPropertyName("street")]
        public string Street { get; set; }
    }
}