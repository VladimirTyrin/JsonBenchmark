﻿using System.Collections.Generic;
using System.Text.Json.Serialization;
using Newtonsoft.Json;

namespace JsonBenchmark.Models
{
    public class PostOfficeListResponse
    {
        [JsonProperty("passportElements")]
        [JsonPropertyName("passportElements")]
        public List<PostOfficeResponse> PassportElements { get; set; }
    }
}
